# Docker Image for Haskell hspec

**Image Location**: `registry.gitlab.com/sebastians90/docker-hspec:latest` (built by [GiltLab CI](https://gitlab.com/SebastianS90/docker-hspec/pipelines))

This Docker image is based on the [haskell image](https://hub.docker.com/_/haskell/) and comes with:

- [hspec](http://hackage.haskell.org/package/hspec)
- [QuickCheck](http://hackage.haskell.org/package/QuickCheck)
- [base-unicode-symbols](http://hackage.haskell.org/package/base-unicode-symbols)

## Contributing
The canonical source of this Docker image is [hosted on GitLab.com](https://gitlab.com/SebastianS90/docker-hspec).
Please file any issues [there](https://gitlab.com/SebastianS90/docker-hspec/issues).

## License
This Dockerfile and the CI configuration is released into the public domain. Please see the [LICENSE](LICENSE) file for details.
For the license of software that is downloaded by this Dockerfile, please refer to their appropriate vendors.
