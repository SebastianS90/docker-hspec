FROM haskell:latest
MAINTAINER Sebastian Schweizer <sebastian@schweizer.tel>

RUN cabal v1-update && cabal v1-install hspec QuickCheck base-unicode-symbols
